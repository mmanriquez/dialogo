(function() {

  angular
    .module('voluntariosApp')
    .controller('homeCtrl', homeCtrl);

    homeCtrl.$inject = ['$http','$sce','$interval','$filter'];
    function homeCtrl ($http,$sce,$interval,$filter) {
      var vm = this;
      var meses = ["Enero", "Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
      var dias = ["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"];
      //var base = "http://citiaps2.diinf.usach.cl/rimay/api";
      var base = "http://159.203.76.103/api";



      //Documentacion servicios REST
      //https://documenter.getpostman.com/view/1786571/rimay/6mz4GL7#dc994309-4b8d-e7a4-7c5b-0169582d3617

      //vm.grupos = [{nombre:"Incendio en Valparaíso",actual:true},{nombre:"Derrumbe",actual:false},{nombre:"Remoción de escombros",actual:false}];
      vm.grupos = [{nombre:"Grupo Onemi",actual:true}];
      vm.grupoActual = 0;
      vm.avanzado = false;
      vm.fechaini = $filter('date')(new Date(),"dd/MM/yyyy");
      vm.fechafin = $filter('date')(new Date(),"dd/MM/yyyy");
      vm.lastAction = "";
      vm.documentos = [
        {_id:"1" ,subido: new Date(2017, 01, 02),nombre:"archivo1.pdf",enviado:[],visto: [],selected: false},
        {_id:"2" ,subido: new Date(2017, 01, 04),nombre:"archivo2.pdf",enviado:[],visto: [],selected: false},
        {_id:"3" ,subido: new Date(2017, 01, 06),nombre:"archivo3.docx",enviado:[],visto: [],selected: false},
        {_id:"4" ,subido: new Date(2017, 01, 07),nombre:"archivo4.pptx",enviado:[],visto: [],selected: false},
        {_id:"5" ,subido: new Date(2017, 01, 08),nombre:"archivo extraño.zip",enviado:[],visto: [],selected: false},
        {_id:"6" ,subido: new Date(2017, 01, 20),nombre:"IMG2017-01-01.jpg",enviado:[],visto: [],selected: false},
        {_id:"7" ,subido: new Date(2017, 01, 22),nombre:"logo01.png",enviado:[],visto: [],selected: false}
      ];
      vm.docs = false;
      vm.contenido = function(array, user){
        if(array){
          for (var i = 0; i < array.length; i++) {
            if(array[i].Id == user.Id)
              return true;
          }
        }
      };

      vm.infoDoc = function(doc,index){
        for (var i = 0; i < vm.documentos.length; i++) {
          vm.documentos[i].selected = false;
        }
        doc.enviado = [];
        doc.visto = [];
        for (var i = 0; i < vm.usuarios.length; i++) {
          if((i+index)%3==0){
            doc.enviado.push(vm.usuarios[i]);
            if((i+index)%2==0){
              doc.visto.push(vm.usuarios[i]);
            }
          }
        }
        doc.subidoString = dias[doc.subido.getDay()] +" "+doc.subido.getDate()+" de "+meses[doc.subido.getMonth()]+", "+doc.subido.getFullYear();
        doc.selected = true;
        vm.docs = true;
        vm.actual = doc;
      };
      vm.getExtension = function(nombre){
        var re = /(?:\.([^.]+))?$/;
        return re.exec(nombre)[1];
      }
      vm.icono = function(nombre){
        var re = /(?:\.([^.]+))?$/;
        var ext = re.exec(nombre)[1];
        switch (ext) {
          case "pdf":
            return "images/docs/pdf.svg"
            break;
          case "doc":
          case "docx":
            return "images/docs/doc.svg"
            break;
          case "ppt":
          case "pptx":
            return "images/docs/ppt.svg"
            break;
          case "xls":
          case "xlsx":
            return "images/docs/xls.svg"
            break;
          case "jpeg":
          case "jpg":
            return "images/docs/jpg.svg"
            break;
          case "png":
            return "images/docs/png.svg"
            break;
          case "mp3":
            return "images/docs/mp3.svg"
            break;
          default:
            return "images/docs/file.svg"

        }
      }


      vm.paginador = {
        paginas : 1,
        actual : 1,
        nppagina : 10,
        max: 10,
        botones:[],
        anterior : function(){
          this.actual--;
          if(this.actual<1){
            this.actual = 1;
          }
          this.crearBotones();
          recargar();
        },
        siguiente : function(){
          this.actual++;
          if(this.actual>=this.paginas){
            this.actual = this.paginas;
          }
          this.crearBotones();
          recargar();
        },
        moverPagina : function(nPagina){
          if(!isNaN(nPagina) && nPagina<=this.paginas && nPagina>=1){
            this.actual = nPagina;
            this.crearBotones();
            recargar();
          }
        },
        crearBotones : function(){
          if(this.paginas<=this.max){
            this.botones = [];
            for (var i = 0; i < this.paginas; i++) {
              this.botones.push(i+1);
            }
          }
          else{
            this.botones = [];
            if(this.actual<this.max){
              var ini = 0;
              var fin = this.max - 1;
              for (var i = ini; i < fin; i++) {
                this.botones.push(i+1);
              }
              this.botones.push("...");
              ini = this.paginas - 2;
              fin = this.paginas;
              for (var i = ini; i < fin; i++) {
                this.botones.push(i+1);
              }
            }
            else if (this.actual>=this.max && this.actual <= (this.paginas-this.max)) {
              var ini = 0;
              var fin = 2;
              for (var i = ini; i < fin; i++) {
                this.botones.push(i+1);
              }
              this.botones.push("...");
              ini = this.actual - 3;
              fin = this.actual + 2;
              for (var i = ini; i < fin; i++) {
                this.botones.push(i+1);
              }
              this.botones.push("...");

              ini = this.paginas - 2;
              fin = this.paginas;
              for (var i = ini; i < fin; i++) {
                this.botones.push(i+1);
              }
            }
            else{
              var ini = 0;
              var fin = 2;
              for (var i = ini; i < fin; i++) {
                this.botones.push(i+1);
              }
              this.botones.push("...");
              ini = this.paginas - this.max;
              fin = this.paginas;
              for (var i = ini; i < fin; i++) {
                this.botones.push(i+1);
              }
            }
          }
        },
        setPaginas: function(n){
          this.paginas = n;
          this.crearBotones();
        }
      };
      /*vm.mensajesPPagina = 10;
      vm.pagActual = 0;
      vm.paginas = 1;*/


      vm.alertas = [];
      vm.alertaSeleccionada = 0;

      //Tipos de estados posibles para una emergencia
      //http://158.170.35.88:4000/emEstado/
      //y parearlo con el atributo Emergencia_estados_id de cada emergencia

      function pedirAlertas(){
          $http.get(base+"/emergencia/")
          .success(function(response){
            //vm.alertas = [{Id:-1,Nombre:"Tareas Generales",Fecha_crear:null,Fecha_terminar:null,actual:true}];
            //vm.alertas = vm.alertas.concat(response);
            vm.alertas = response;
            for (var i = 0; i < vm.alertas.length; i++) {
              vm.alertas[i].actual = false;
            }
            vm.alertas[vm.alertaSeleccionada].actual = true;
          })
          .error(function(errData){
            console.log("ERROR:"+errData);
          });
      }

      vm.usuarios = [];
      vm.usuarioActual;

      vm.cambiarAlerta = function(alerta){
        vm.alertas[vm.alertaSeleccionada].actual = false;
        vm.alertaSeleccionada = alerta;
        vm.alertas[vm.alertaSeleccionada].actual = true;
        /*for (var i = 0; i < vm.alertas.length; i++) {
          vm.alertas[i].actual = false;
          if(i==alerta){
            vm.alertas[i].actual = true;
            vm.alertaSeleccionada = alerta;
          }
        }*/
        mensajesPorAlerta();
      };

      function mensajesPorAlerta(){
        if(vm.lastAction!="emergencia"){
          vm.paginador.actual = 1;
        }
        vm.lastAction = "emergencia";
        vm.query = "";
        vm.querySend = "";
        //pedir cantidad de paginas de estos mensajes
        pedirPaginasEmergencia(vm.alertas[vm.alertaSeleccionada].Id);

        //vm.paginador.setPaginas(1);
        $http.get(base+"/mensajes/filtro",{params:{emergencia:vm.alertas[vm.alertaSeleccionada].Id,offset:vm.paginador.nppagina*(vm.paginador.actual-1),limit:vm.paginador.nppagina}})
        .success(exitoMensajes)
        .error(function(err){
          vm.vacio = false;
          console.log("ERROR: Al filtrar por emergencia");
          console.log(err);
        });
      }


      vm.mensajes = [];
      function pedirMensajes(){
        pedirPaginas();
        if(vm.lastAction!="mensajes"){
          vm.paginador.actual = 1;
        }
        vm.lastAction = "mensajes";
        $http.get(base+"/mensajes/mostrar",{params:{offset:vm.paginador.nppagina*(vm.paginador.actual-1),limit:vm.paginador.nppagina}})
        .success(exitoMensajes)
        .error(function(err){
          vm.vacio = false;
          console.log("ERROR: Al recuperar mensajes");
          console.log(err);
        });
      }

      vm.armaConsulta = function(user){
        console.log("pidiendo mensajes de ...");
        console.log(user);
        for (var i = 0; i < vm.usuarios.length; i++) {
          if(vm.usuarios[i].Id == user.Id){
            vm.usuarioActual = i;
          }
        }
        console.log("Usuario actual"+vm.usuarioActual);
        vm.lastAction = "porUsuario";

        mensajesPorUsuario();
      }

      function mensajesPorUsuario(){
        //pedir paginas para mensajes por usuario
        $http.get(base+"/mensajes/usuario/rangos",{params:{id:vm.usuarios[vm.usuarioActual].Id,limit:vm.paginador.nppagina}})
        .success(exitoPaginas);

        //pedir mensajes por usuario
        $http.get(base+"/mensajes/usuario",{params:{id:vm.usuarios[vm.usuarioActual].Id,offset:vm.paginador.nppagina*(vm.paginador.actual-1),limit:vm.paginador.nppagina}})
        .success(exitoMensajes);
      }


      vm.buscar = function(query){
        if(query.trim()!=""){
          vm.querySend = query;
          pedirPaginasQuery(vm.querySend);
          vm.lastAction = "query";

          //Seleccionar la alerta general
          vm.alertas[vm.alertaSeleccionada].actual = false;
          vm.alertaSeleccionada = 0;
          vm.alertas[vm.alertaSeleccionada].actual = true;

          $http.get(base+"/mensajes/busqueda",
            {
              params:{
                q:vm.querySend,
                limit:vm.paginador.nppagina,
                offset:(vm.paginador.actual-1)*vm.paginador.nppagina
              }
            })
          .success(exitoMensajes)
          .error(function(errdata){
            if(errdata=="Not found"){
              vm.vacio= true;
            }
          });
        }
        else{
          vm.querySend = "";
          pedirMensajes();
        }
      }
      function pedirPaginas(){
        //vm.paginador.setPaginas(30);
        $http.get(base+"/mensajes/rangos",{params:{limit:vm.paginador.nppagina}})
        .success(exitoPaginas);
      }

      function pedirPaginasQuery(consulta){
        $http.get(base+"/mensajes/busqueda/rangos",{params:{q:consulta,limit:vm.paginador.nppagina}})
        .success(exitoPaginas);
      }

      function pedirPaginasEmergencia(emergenciaId){
        //mensajes/filtro/rangos?emergencia=1&offset=0&limit=10
        $http.get(base+"/mensajes/filtro/rangos",{params:{emergencia: emergenciaId, limit:vm.paginador.nppagina}})
        .success(exitoPaginas);
      }

      function exitoPaginas(response){
        if(!response.cant){
          vm.paginador.setPaginas(1);
        }
        else{
          if((response.cant)!=vm.paginador.paginas){
            vm.paginador.setPaginas(response.cant);
            if(vm.paginador.actual > vm.paginador.paginas){
              vm.paginador.actual = 1;
            }
          }
        }
      }


      function exitoMensajes(response){
        if(!response || response==null){
          vm.vacio = true;
        }
        else{
          response.sort(compararMensajes);
          vm.mensajes = response;
          if(vm.mensajes.length==0){
            vm.vacio = true;
          }
          else{
            var anterior = new Date(2000,1,1);
            vm.vacio = false;
            for (var i = 0; i < vm.mensajes.length; i++) {
              vm.mensajes[i].Texto = anchorme(vm.mensajes[i].Texto,{attributes:[{name:"target",value:"blank"}]});
              vm.mensajes[i].Texto = resaltar(vm.mensajes[i].Texto);
              if(vm.mensajes[i].destacado){
                vm.destacados.push(vm.mensajes[i]);
              }
              var aux = new Date(vm.mensajes[i].Create_at);
              vm.mensajes[i].Datext = dias[aux.getDay()] +" "+aux.getDate()+" de "+meses[aux.getMonth()]+", "+aux.getFullYear();
              vm.mensajes[i].detalle = $filter('date')(aux,"HH:mm:ss")
              //console.log(Math.abs(getDiffDays(aux,anterior)));
              vm.mensajes[i].showDate = (getDiffDays(aux,anterior));
              anterior = aux;
            }
          }
        }
      }

      function resaltar(texto){
        texto = texto.replace(/\ \/\S*/gi, function(palabra){ return "<strong class='slash'> "+palabra.substring(1)+"</strong>"});
        if(texto[0]=='/'){
          texto = texto.replace(/\/\S*/gi, function(palabra){ return "<strong class='slash'> "+palabra+"</strong>"});
        }
        texto = texto.replace(/@\S*/gi, function(palabra){ return "<strong class='arroba'>"+palabra+"</strong>"});
        texto = texto.replace(/#\S*/gi, function(palabra){ return "<strong class='hashtag'>"+palabra+"</strong>"});
        texto = texto.replace(/\n/g,"<br/>");

        //console.log(texto);
        return $sce.trustAsHtml(texto);
      }


      function getDiffDays(a, b){
        //var diff =  Math.floor((a.getTime()-b.getTime())/(1000 * 60 * 60 * 24));
        var valor = a.getFullYear()==b.getFullYear() && a.getMonth()==b.getMonth() && a.getDate()==b.getDate();
        return !valor;
      }


      //Esta función es una modificación de lo publicado por Adam Cole (https://stackoverflow.com/users/945065/adam-cole) en
      //stackoverflow https://stackoverflow.com/questions/1484506/random-color-generator
      //se le agregó el factor 'p' para hacer los colores un poco más oscuros, y
      //el parametro de brillo para hacer los colores un poco más pasteles.
      function rainbow(numOfSteps, step, brillo) {
          var r, g, b;
          var h = step / numOfSteps;
          var i = ~~(h * 6);
          var f = h * 6 - i;
          var q = 1 - f;
          var p = 0.75;//1 --> muy saturado. 0 --> nada de saturación
          switch(i % 6){
              case 0: r = p; g = f; b = 0; break;
              case 1: r = q; g = p; b = 0; break;
              case 2: r = 0; g = p; b = f; break;
              case 3: r = 0; g = q; b = p; break;
              case 4: r = f; g = 0; b = p; break;
              case 5: r = p; g = 0; b = q; break;
          }
          var c = "rgba("+(~ ~(r * 255))+","+(~ ~(g * 255))+","+(~ ~(b * 255))+","+brillo+")";
          return (c);
      }

      vm.getUserStyle = function(userId){
        for (var i = 0; i < vm.usuarios.length; i++) {
          if(vm.usuarios[i].Id==userId ){
            //console.log(username +" es igual a "+vm.usuarios[i].User);
            return vm.usuarios[i].style;
          }
        }
        return "background-color: #444";
      }
      vm.getUsername = function(userId){
        for (var i = 0; i < vm.usuarios.length; i++) {
          if(vm.usuarios[i].Id==userId ){
            //console.log(username +" es igual a "+vm.usuarios[i].User);
            return vm.usuarios[i]._name;
          }
        }
      }

      function pedirUsuarios(){
        //console.log("pidiendo usuarios");
        $http.get(base+'/usuario/')
        .success(function(response){
          for (var i = 0; i < response.length; i++) {
            response[i].style='background-color:'+rainbow(10, i,0.8)+";";
            response[i]._name = response[i].First_name + " " + response[i].Last_name;
            if(response[i]._name.trim()==""){
              response[i]._name = response[i].Username;
              if(response[i]._name.trim()==""){
                response[i]._name = response[i].Id;
              }
            }
          }
          response.sort(function(a, b){
            if(a._name < b._name) return -1;
            if(a._name > b._name) return 1;
            return 0;
          });
          vm.usuarios = response;
        });
      }

      vm.vacio= false;
      vm.query = "";
      vm.querySend = "";


      vm.destacados = [];
      vm.destacar = function(message){
        message.destacado = !message.destacado;
        var pos = vm.destacados.indexOf(message);
        if(pos>=0){
          vm.destacados.splice(pos,1);
        }
        else{
          vm.destacados.push(message);
        }
      }

      function recargar(){
        switch (vm.lastAction) {
          case "query":
            if(vm.querySend!=""){
              vm.buscar(vm.querySend);
            }
            break;
          case "mensajes":
            pedirMensajes();
            break;
          case "emergencia":
            mensajesPorAlerta();
            break;
          case "porUsuario":
            mensajesPorUsuario();
            break;
          default:
            pedirMensajes();
            break;
        }

        /*if(vm.querySend!=""){
          vm.buscar(vm.querySend);
        }
        else{
          pedirMensajes();
          //pedirPaginas();
        }*/
        pedirUsuarios();
        pedirAlertas();
      }

      function compararMensajes(a, b){
        var x = new Date(a.Create_at);
        var y = new Date(b.Create_at);
        return y.getTime()-x.getTime();
      }
      recargar();//la llamo la primera vez, no espera 5 segundos

      $interval(recargar, 5000,-1);//luego de eso espera 5 segundos para cada llamada

    }

})();
