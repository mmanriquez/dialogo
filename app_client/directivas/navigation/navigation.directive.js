(function () {


  //pongo unos acentos para forzar a que guarde en utf8 áéíóú
  angular
    .module('voluntariosApp')
    .directive('navigation', navigation);

  function navigation () {
    return {
      restrict: 'EA',
      templateUrl: './directivas/navigation/navigation.template.html',
      controller: 'navigationCtrl as navvm'
    };
  }

})();
