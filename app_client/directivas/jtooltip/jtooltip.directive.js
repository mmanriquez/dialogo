(function () {

  angular
    .module('voluntariosApp')
    .directive('jtooltip', jtooltip);

  function jtooltip() {
    return {
      restrict: 'A',
      link: function(scope, element, attrs, ctrl) {
          element.tooltip({
            classes: {
              "ui-tooltip": "jtooltip",
              "ui-tooltip-content":"jtooltip-content"
            }
          });
      }
    }
  }

})();
