(function () {
  angular
  .module("voluntariosApp")
  .directive("scrollToBottom",scrollToBottom);

  function scrollToBottom() {
    function link (scope, element, attrs) {
      scope.$on(attrs.scrollToBottom, function () {
        angular.element(element)[0].scrollTop = angular.element(element)[0].scrollHeight;
      });
    }
  }
})();
