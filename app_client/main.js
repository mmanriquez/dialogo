(function () {

  angular.module('voluntariosApp', ['ngRoute','ngSanitize']);

  function config ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'vistas/home.view.html',
        controller: 'homeCtrl',
        controllerAs: 'vm'
      })
      .otherwise({redirectTo: '/'});

    // use the HTML5 History API
    //$locationProvider.html5Mode(true);
  }

  function run($rootScope, $location) {
    console.log("en Run");
  }

  angular
    .module('voluntariosApp')
    .config(['$routeProvider', '$locationProvider', config])
    .run(['$rootScope', '$location', run]);

})();
